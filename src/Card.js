import React from "react";

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      number: "",
      name: "",
      cvv: "",
      month: "",
      year: ""
    };

    // This binding is necessary to make `this` work in the callback
	this.handleNumber = this.handleNumber.bind(this);
	this.handleName = this.handleName.bind(this);
	this.handleMonth = this.handleMonth.bind(this);
	this.handleYear = this.handleYear.bind(this);
	this.handleCVV = this.handleCVV.bind(this);
  }

  handleNumber(e) {
    this.setState({number: e.target.value});
  }
  handleName(e) {
    this.setState({name: e.target.value});
  }
  handleMonth(e) {
    this.setState({month: e.target.value});
  }
  handleYear(e) {
    this.setState({year: e.target.value});
  }
  handleCVV(e) {
    this.setState({cvv: e.target.value});
  }

  render() {
    const number = this.state.number;
    const name = this.state.name;
    const cvv = this.state.cvv;
    const month = this.state.month;
    const year = this.state.year;

    return (
      <div className="cardContainer">
        <div className="card">
		  <h1>Tarjeta</h1>
          <p>Numero: {number}</p>
          <p>Nombre: {name}</p>
          <p>CVV: {cvv}</p>
          <p>Mes: {month}</p>
          <p>Año: {year}</p>
        </div>
        <div className="formCard">
          <fieldset>
            <legend>Numero</legend>
            <input type="number" value={number} onChange={this.handleNumber} />
          </fieldset>
		  <fieldset>
            <legend>Nombre</legend>
            <input type="text" value={name} onChange={this.handleName} />
          </fieldset>
		  <fieldset>
            <legend>Mes</legend>
            <input type="text" value={month} onChange={this.handleMonth} />
          </fieldset>
		  <fieldset>
            <legend>Año</legend>
            <input type="text" value={year} onChange={this.handleYear} />
          </fieldset>
		  <fieldset>
            <legend>CVV</legend>
            <input type="text" value={cvv} onChange={this.handleCVV} />
          </fieldset>
        </div>
      </div>
    );
  }
}

export default Card;
